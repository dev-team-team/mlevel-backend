package com.mlevel.backend.api;


import com.mlevel.backend.api.timeline.ActionDTO;
import com.mlevel.backend.api.timeline.CoverDTO;
import com.mlevel.backend.api.timeline.TimelineItemDTO;

import java.util.List;

/**
 * Created by agustin on 16/03/16.
 */
public class EventDTO extends TimelineItemDTO {

    private String facebookId;

    private CoverDTO coverDTO;

    private String eventType;

    private List<ActionDTO> commonActions;

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public TimelineItemType getType() {
        return TimelineItemType.EVENT;
    }

    public CoverDTO getCoverDTO() {
        return coverDTO;
    }

    public void setCoverDTO(CoverDTO coverDTO) {
        this.coverDTO = coverDTO;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public List<ActionDTO> getCommonActions() {
        return commonActions;
    }

    public void setCommonActions(List<ActionDTO> commonActions) {
        this.commonActions = commonActions;
    }
}
