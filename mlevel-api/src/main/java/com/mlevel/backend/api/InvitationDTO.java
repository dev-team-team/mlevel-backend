package com.mlevel.backend.api;

import com.mlevel.backend.api.timeline.ActionDTO;

import java.util.Date;

/**
 * Created by agustin on 17/03/16.
 */
public class InvitationDTO extends AbstractDTO {

    public enum State { ACTIVE, ACCEPTED, DELETED,  CLOSED }

    public Long id;

    public Long senderId;

    public Long receiverId;

    public Date timestamp;

    public Date startTime;

    public Date endTime;

    public Long eventId;

    public ActionDTO action;

    public State state;

}
