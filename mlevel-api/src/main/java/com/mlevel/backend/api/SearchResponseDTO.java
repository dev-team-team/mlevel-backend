package com.mlevel.backend.api;

import java.util.List;

/**
 * Created by agustin on 26/05/16.
 */
public class SearchResponseDTO extends AbstractDTO {

	public List results;

	public int total;

	public SearchResponseDTO(List results, int total){
		this.results = results;
		this.total = total;
	}
}
