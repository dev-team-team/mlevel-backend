package com.mlevel.backend.api.authentication;

/**
 * Created by agustin on 06/03/16.
 */
public class AuthenticatedUserTokenDTO {

    private Long userId;
    private String token;

    public AuthenticatedUserTokenDTO(){}

    public AuthenticatedUserTokenDTO(Long userId, String sessionToken) {
        this.userId = userId;
        this.token = sessionToken;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
