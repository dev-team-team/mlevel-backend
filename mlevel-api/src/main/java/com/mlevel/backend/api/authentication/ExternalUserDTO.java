package com.mlevel.backend.api.authentication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by agustin on 06/03/16.
 */
public class ExternalUserDTO {

    private Long id;

    private String name;

    private String lastName;

    private String email;

    private boolean isVerified;

    private String role;

    private List<SocialProfileDTO> socialProfiles = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<SocialProfileDTO> getSocialProfiles() {
        return socialProfiles;
    }

    public void setSocialProfiles(List<SocialProfileDTO> socialProfiles) {
        this.socialProfiles = socialProfiles;
    }
}