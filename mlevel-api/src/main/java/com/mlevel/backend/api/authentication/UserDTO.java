package com.mlevel.backend.api.authentication;

import com.mlevel.backend.api.AbstractDTO;

import java.util.Date;

public class UserDTO extends AbstractDTO {

    private Long id;

    private String uuid;

    private String name;

    private String lastName;

    private Date birthday;

    private String email;

    private boolean favouriteUser;

    private Date lastConnection;

    public boolean isFavouriteUser() {
        return favouriteUser;
    }

    public void setFavouriteUser(boolean favouriteUser) {
        this.favouriteUser = favouriteUser;
    }

    public Date getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(Date lastConnection) {
        this.lastConnection = lastConnection;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
