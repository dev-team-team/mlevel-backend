package com.mlevel.backend.api.authentication.request;


import com.mlevel.backend.api.authentication.ExternalUserDTO;

public class CreateUserRequestDTO {

    private ExternalUserDTO user;

    private PasswordRequestDTO password;

    public CreateUserRequestDTO() {
    }

    public CreateUserRequestDTO(final ExternalUserDTO user, final PasswordRequestDTO password) {
        this.user = user;
        this.password = password;
    }

    public ExternalUserDTO getUser() {
        return user;
    }

    public void setUser(ExternalUserDTO user) {
        this.user = user;
    }

    public PasswordRequestDTO getPassword() {
        return password;
    }

    public void setPassword(PasswordRequestDTO password) {
        this.password = password;
    }
}
