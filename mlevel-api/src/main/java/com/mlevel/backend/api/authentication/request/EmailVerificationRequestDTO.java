package com.mlevel.backend.api.authentication.request;

/**
 * Created by agustin on 06/03/16.
 */
public class EmailVerificationRequestDTO {

    private String emailAddress;

    public EmailVerificationRequestDTO() {}

    public EmailVerificationRequestDTO(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
