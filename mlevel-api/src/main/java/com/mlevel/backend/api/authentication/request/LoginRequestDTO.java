package com.mlevel.backend.api.authentication.request;


import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 06/03/16.
 */
public class LoginRequestDTO extends AbstractDTO {

    private String username;

    private String password;

    public LoginRequestDTO(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
