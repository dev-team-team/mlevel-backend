package com.mlevel.backend.api.authentication.request;

/**
 * Created by agustin on 06/03/16.
 */
public class LostPasswordRequestDTO {

    private String emailAddress;

    public LostPasswordRequestDTO() {}

    public LostPasswordRequestDTO(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
