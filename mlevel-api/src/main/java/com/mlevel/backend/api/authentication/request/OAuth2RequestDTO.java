package com.mlevel.backend.api.authentication.request;


import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 06/03/16.
 */
public class OAuth2RequestDTO extends AbstractDTO {

    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
