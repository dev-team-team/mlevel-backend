package com.mlevel.backend.api.blob;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 20/05/16.
 */
public class BlobUrlDTO extends AbstractDTO {

	private String url;

	public BlobUrlDTO(){}

	public BlobUrlDTO(String url){
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
