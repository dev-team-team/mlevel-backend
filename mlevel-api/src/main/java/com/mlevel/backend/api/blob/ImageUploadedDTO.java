package com.mlevel.backend.api.blob;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 20/05/16.
 */
public class ImageUploadedDTO extends AbstractDTO {

	private String servingUrl;

	private String blobKey;

	public String getServingUrl() {
		return servingUrl;
	}

	public void setServingUrl(String servingUrl) {
		this.servingUrl = servingUrl;
	}

	public String getBlobKey() {
		return blobKey;
	}

	public void setBlobKey(String blobKey) {
		this.blobKey = blobKey;
	}
}
