package com.mlevel.backend.api.location;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 10/03/16.
 */
public class LocationDTO extends AbstractDTO {

    private double latitude;

    private double longitude;

    private String city;

    private String country;

    private String address;

    public LocationDTO() {
    }

    public LocationDTO(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
