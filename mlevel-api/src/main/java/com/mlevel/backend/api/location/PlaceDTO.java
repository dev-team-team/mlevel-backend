package com.mlevel.backend.api.location;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by macuco on 23/4/16.
 */
public class PlaceDTO extends AbstractDTO {

    private String uuid;

    private  String facebookId;

    private String name;

    private LocationDTO location;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }
}
