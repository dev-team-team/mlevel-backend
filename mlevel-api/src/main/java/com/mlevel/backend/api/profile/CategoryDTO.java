package com.mlevel.backend.api.profile;

import com.mlevel.backend.api.AbstractDTO;

public class CategoryDTO extends AbstractDTO {

    private String name;

    public CategoryDTO() {
    }

    public CategoryDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
