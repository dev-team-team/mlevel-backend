package com.mlevel.backend.api.profile;

import com.mlevel.backend.api.AbstractDTO;

import java.util.List;

/**
 * Created by agustin on 20/05/16.
 */
public class CategoryRequestDTO extends AbstractDTO {

	private List<CategoryDTO> categories;

	public List<CategoryDTO> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryDTO> categories) {
		this.categories = categories;
	}
}
