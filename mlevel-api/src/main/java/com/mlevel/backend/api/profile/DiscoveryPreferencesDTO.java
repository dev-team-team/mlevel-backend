package com.mlevel.backend.api.profile;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 19/05/16.
 */
public class DiscoveryPreferencesDTO extends AbstractDTO {

	// true = quiero ser encontrado por gente desconocida del sexo seleccionado.
	private boolean discovery;

	// true = quiero poder ser encontrado y agregado como favorito
	// Capaz me interesa que nadie sepa que tengo la app instalada y solo la quiero para yo invitar.
	private boolean findable;

	// Rango de edades que me interesa
	private int minAge;

	private int maxAge;

	// Sexo de los desconocidos que me interesa - (Men, Woman, Both)
	private String sex;

	public boolean isDiscovery() {
		return discovery;
	}

	public void setDiscovery(boolean discovery) {
		this.discovery = discovery;
	}

	public boolean isFindable() {
		return findable;
	}

	public void setFindable(boolean findable) {
		this.findable = findable;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
