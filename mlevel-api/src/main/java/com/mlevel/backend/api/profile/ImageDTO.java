package com.mlevel.backend.api.profile;

/**
 * Created by agustin on 19/05/16.
 */
public class ImageDTO {

	private String source;

	private double offsetX;

	private double offsetY;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public double getOffsetX() {
		return offsetX;
	}

	public void setOffsetX(double offsetX) {
		this.offsetX = offsetX;
	}

	public double getOffsetY() {
		return offsetY;
	}

	public void setOffsetY(double offsetY) {
		this.offsetY = offsetY;
	}
}
