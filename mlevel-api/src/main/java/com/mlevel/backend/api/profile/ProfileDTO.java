package com.mlevel.backend.api.profile;

import com.mlevel.backend.api.AbstractDTO;

import java.util.List;

/**
 * Created by agustin on 19/05/16.
 */
public class ProfileDTO extends AbstractDTO {

	private Long id;

	// Categorias de interes para el muro de eventos
	private List<CategoryDTO> categories;

	private DiscoveryPreferencesDTO discoveryPreferencesDTO;

	private String nickName;

	private String shortMessage;

	// User sex (Man, Woman)
	private String sex;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CategoryDTO> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryDTO> categories) {
		this.categories = categories;
	}

	public DiscoveryPreferencesDTO getDiscoveryPreferencesDTO() {
		return discoveryPreferencesDTO;
	}

	public void setDiscoveryPreferencesDTO(DiscoveryPreferencesDTO discoveryPreferencesDTO) {
		this.discoveryPreferencesDTO = discoveryPreferencesDTO;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getShortMessage() {
		return shortMessage;
	}

	public void setShortMessage(String shortMessage) {
		this.shortMessage = shortMessage;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
