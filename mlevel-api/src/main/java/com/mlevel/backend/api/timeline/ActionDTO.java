package com.mlevel.backend.api.timeline;

import com.mlevel.backend.api.AbstractDTO;

public class ActionDTO extends AbstractDTO {

    private String name;

    private String icon;

    public ActionDTO(){}

    public ActionDTO(String name, int imgUrl){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
