package com.mlevel.backend.api.timeline;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 23/04/16.
 */
public class CoverDTO extends AbstractDTO {

    private Long id;

    private String facebookId;

    private String source;

    private double offsetX;

    private double offsetY;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public double getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(double offsetX) {
        this.offsetX = offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(double offsetY) {
        this.offsetY = offsetY;
    }
}
