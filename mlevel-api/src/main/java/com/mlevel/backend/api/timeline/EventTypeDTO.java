package com.mlevel.backend.api.timeline;

import com.mlevel.backend.api.AbstractDTO;

/**
 * Created by agustin on 16/03/16.
 */
public class EventTypeDTO extends AbstractDTO {

    public enum EventType{
        PROMO_AEREA,
        FOOTBALL_MATCH,
        CINEMA_FILM,
        CONCERT
    }

    private String name;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
