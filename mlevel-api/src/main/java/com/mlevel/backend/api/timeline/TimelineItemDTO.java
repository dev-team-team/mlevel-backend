package com.mlevel.backend.api.timeline;


import com.mlevel.backend.api.AbstractDTO;
import com.mlevel.backend.api.location.PlaceDTO;

import java.util.Date;

/**
 * Created by agustin on 02/04/16.
 */
public abstract class TimelineItemDTO extends AbstractDTO {

    private Long id;

    private String name;

    private String subtitle;

    private String description;

    private PlaceDTO placeDTO;

    private Date startTime;

    private Date endTime;

    private Date creationDate;

    public abstract TimelineItemType getType();

    public enum TimelineItemType {
        EVENT, OPEN_INVITATION
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlaceDTO getPlaceDTO() {
        return placeDTO;
    }

    public void setPlaceDTO(PlaceDTO placeDTO) {
        this.placeDTO = placeDTO;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
