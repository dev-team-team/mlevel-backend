package com.mlevel.backend.service;

import com.google.inject.Singleton;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;

import static com.mlevel.backend.service.OfyService.ofy;

@Singleton
public class AuthorizationTokenService {

	public AuthorizationToken loadAuthorizationToken(String token){
		return ofy().load().type(AuthorizationToken.class).filter("token", token).first().now();
	}
}
