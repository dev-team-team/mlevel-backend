package com.mlevel.backend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Preconditions;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskHandle;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.inject.Singleton;
import com.mlevel.backend.service.model.Event;
import com.mlevel.backend.service.util.Constants;

import java.util.List;
import java.util.logging.Logger;

import static com.mlevel.backend.service.OfyService.ofy;

@Singleton
public class EventService {

	private static final Logger LOGGER = Logger.getLogger(EventService.class.getName());
	private ObjectMapper objectMapper = new ObjectMapper();

	public String createEvent(Event event){
		ObjectMapper objectMapper = new ObjectMapper();
		LOGGER.info("Validating eventDTO creation");

		Preconditions.checkNotNull(event);
		Preconditions.checkNotNull(event.getType());
		Preconditions.checkNotNull(event.getName());
		Preconditions.checkNotNull(event.getSubtitle());
		Preconditions.checkNotNull(event.getDescription());
		Preconditions.checkNotNull(event.getCover());
		Preconditions.checkNotNull(event.getStartTime());
		Preconditions.checkNotNull(event.getEndTime());
		Preconditions.checkNotNull(event.getPlace());
		Preconditions.checkNotNull(event.getPlace().getValue().getLocation());
		Preconditions.checkNotNull(event.getCommonActions());

		LOGGER.info("Creating New Event");

		// [START get_queue]
		Queue eventsQueue = QueueFactory.getQueue(Constants.INDEX_EVENT_QUEUE);

		LOGGER.info("Saving event in DataStore...");
		// Save the event in the DS
		ofy().save().entity(event);
		try {
			LOGGER.info("Adding event to Index Queue...");
			// add index event in elastic
			eventsQueue.add(TaskOptions.Builder.withMethod(TaskOptions.Method.PULL)
				.payload(objectMapper.writeValueAsString(event)));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return "aaa";
	}

	public void indexEvents(List<TaskHandle> eventTasks, Queue eventsQueue) {
		try {
			//Event event = objectMapper.readValue(new String(task.getPayload()), Event.class);
			LOGGER.info("Indexing event...");

			LOGGER.info("Event indexed");
			List<Boolean> deletedTasks = eventsQueue.deleteTask(eventTasks);
			for (Boolean operationSucceed : deletedTasks){
				if(!operationSucceed){
					LOGGER.info("Event was not deleted");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
