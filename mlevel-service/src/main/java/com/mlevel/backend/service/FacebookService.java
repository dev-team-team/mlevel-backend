package com.mlevel.backend.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.service.rest.FacebookRestClient;
import com.mlevel.backend.service.rest.dto.DebugTokenDTO;
import com.mlevel.backend.service.rest.dto.PublicProfileDTO;

@Singleton
public class FacebookService {

	@Inject
	private FacebookRestClient restClient;

	public DebugTokenDTO debugAccessToken(String accessToken){
		DebugTokenDTO debugTokenDTO = restClient.debugUserAccessToken(accessToken);
		return debugTokenDTO;
	}

	public PublicProfileDTO getUserPublicProfile(String userId){
		PublicProfileDTO publicProfileDTO = restClient.getUserPublicProfile(userId);
		return publicProfileDTO;
	}
}
