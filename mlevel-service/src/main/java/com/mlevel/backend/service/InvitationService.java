package com.mlevel.backend.service;

import com.google.api.client.util.Preconditions;
import com.google.appengine.repackaged.com.google.common.base.Pair;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.mlevel.backend.service.model.Invitation;
import com.mlevel.backend.service.model.User;

import java.util.List;
import java.util.logging.Logger;

import static com.mlevel.backend.service.OfyService.ofy;

/**
 * Created by agustin on 29/05/16.
 */
@SuppressWarnings("ALL")
@Singleton
public class InvitationService {

	private static final Logger LOGGER = Logger.getLogger(InvitationService.class.getName());

	public Invitation createInvitation(Invitation invitation){
		Preconditions.checkNotNull(invitation);
		Preconditions.checkNotNull(invitation.getSender());
		Preconditions.checkNotNull(invitation.getReceiver());
		Preconditions.checkNotNull(invitation.getAction());
		Preconditions.checkNotNull(invitation.getStartTime());
		Preconditions.checkNotNull(invitation.getEndTime());
		Key<Invitation> invitationKey = ofy().save().entity(invitation).now();
		invitation.setId(invitationKey.getId());
		return invitation;
	}

	public Boolean updateInvitationState(Long invitationId, Invitation.State state){
		try {
			Preconditions.checkNotNull(invitationId);
			Invitation invitation = ofy().load().type(Invitation.class).id(invitationId).now();
			invitation.setState(state);
			ofy().save().entity(invitation).now();
			return true;
		} catch (Exception e){
			LOGGER.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Load invitations made TO the user
	 * @param userId
	 * @return
	 */
	public Pair<Integer, List<Invitation>> loadUserInvitations(Long userId, int from){
		Preconditions.checkNotNull(userId);
		LOGGER.info("loading user invitations");
		int total =  ofy().load().type(Invitation.class).filter("receiver", Key.create(User.class, userId)).count();
		List<Invitation> invitations =  ofy().load().type(Invitation.class).filter("receiver", Key.create(User.class, userId)).limit(20).offset(from).list();
		return Pair.of(total, invitations);
	}

	/**
	 * Load invitations the user MADE
	 * @param userId
	 * @return
	 */
	public Pair<Integer, List<Invitation>> loadUserInvites(Long userId, int from){
		Preconditions.checkNotNull(userId);
		LOGGER.info("loading user invitations");
		int total =  ofy().load().type(Invitation.class).filter("sender", Key.create(User.class, userId)).count();
		List<Invitation> invitations =  ofy().load().type(Invitation.class).filter("sender", Key.create(User.class, userId)).limit(20).offset(from).list();
		return Pair.of(total, invitations);
	}
}
