package com.mlevel.backend.service;

import com.google.inject.Singleton;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.mlevel.backend.service.model.*;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;

@Singleton
public class OfyService {

	static {
		ObjectifyService.factory().register(TimelineItem.class);
		ObjectifyService.factory().register(Event.class);
		ObjectifyService.factory().register(Place.class);
		ObjectifyService.factory().register(Cover.class);
		ObjectifyService.factory().register(User.class);
		ObjectifyService.factory().register(Profile.class);
		ObjectifyService.factory().register(AuthorizationToken.class);
	}

	public static Objectify ofy() {
		return ObjectifyService.ofy();
	}

	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}
