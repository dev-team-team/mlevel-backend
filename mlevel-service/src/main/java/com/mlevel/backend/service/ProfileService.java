package com.mlevel.backend.service;

import com.google.api.client.util.Preconditions;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Work;
import com.mlevel.backend.api.profile.CategoryDTO;
import com.mlevel.backend.service.model.Profile;
import com.mlevel.backend.service.model.User;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;

import java.util.List;
import java.util.logging.Logger;

import static com.mlevel.backend.service.OfyService.ofy;

@Singleton
public class ProfileService {

	private static final Logger LOGGER = Logger.getLogger(ProfileService.class.getName());

	public Profile updateUserCategories(final Profile profile){
		Preconditions.checkNotNull(profile);
		Preconditions.checkNotNull(profile.getCategories());
		Preconditions.checkNotNull(profile.getUser());
		final Profile userProfile = ofy().load().type(Profile.class).filter("user", profile.getUser().key()).first().now();
		if(userProfile == null) {
			//First time, create the profile
			//TODO check que el usuario no tenga perfiles asociados
			LOGGER.info("Creating Profile - Setting User Categories");
			Key<Profile> profileKey = ofy().save().entity(profile).now();
			profile.setId(profileKey.getId());
			return profile;
		} else {
			// Load and update
			LOGGER.info("Updating User categories for user " + profile.getId());
			userProfile.setCategories(profile.getCategories());
			ofy().save().entity(userProfile).now();
			return userProfile;
		}
	}

	public Profile updateUserPreferences(Profile profile){

		Preconditions.checkNotNull(profile);
		Preconditions.checkNotNull(profile.getDiscoveryPreferences());
		Preconditions.checkNotNull(profile.getDiscoveryPreferences().getSex());
		Preconditions.checkNotNull(profile.getDiscoveryPreferences().getMinAge());
		Preconditions.checkNotNull(profile.getDiscoveryPreferences().getMaxAge());
		Preconditions.checkNotNull(profile.getDiscoveryPreferences().isDiscovery());
		Preconditions.checkNotNull(profile.getDiscoveryPreferences().isFindable());
		Preconditions.checkNotNull(profile.getUser());
		Preconditions.checkNotNull(profile.getNickName());
		Preconditions.checkNotNull(profile.getSex());
		final Profile userProfile = ofy().load().type(Profile.class).filter("user", profile.getUser().key()).first().now();
		if(userProfile == null) {
			//First time, create the profile
			LOGGER.info("Creating Profile - Setting User Preferences");
			Key<Profile> profileKey = ofy().save().entity(profile).now();
			profile.setId(profileKey.getId());
			return profile;
		} else {
			// Load and update
			LOGGER.info("Updating User preferences for user " + profile.getId());
			userProfile.setDiscoveryPreferences(profile.getDiscoveryPreferences());
			userProfile.setSex(profile.getSex());
			userProfile.setShortMessage(profile.getShortMessage());
			userProfile.setNickName(profile.getNickName());
			Key<Profile> profileKey = ofy().save().entity(userProfile).now();
			profile.setId(profileKey.getId());
			return userProfile;
		}

	}

	public Profile getUserProfile(String userId){
		Preconditions.checkNotNull(userId);
		return ofy().load().type(Profile.class).filter("user", Key.create(User.class, userId)).first().now();
	}

}
