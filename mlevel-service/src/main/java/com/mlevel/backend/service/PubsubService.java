package com.mlevel.backend.service;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpStatusCodes;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.PushConfig;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.mlevel.backend.service.util.PubsubUtils;

import java.io.IOException;

/**
 * Created by agustin on 14/05/16.
 */
public class PubsubService {

//	/**
//	 * Creates a Cloud Pub/Sub topic if it doesn't exist.
//	 *
//	 * @param client Pubsub client object.
//	 * @throws IOException when API calls to Cloud Pub/Sub fails.
//	 */
//	private void setupTopic(final Pubsub client) throws IOException {
//		String fullName = String.format("projects/%s/topics/%s",
//			PubsubUtils.getProjectId(),
//			PubsubUtils.getAppTopicName());
//
//		try {
//			client.projects().topics().get(fullName).execute();
//		} catch (GoogleJsonResponseException e) {
//			if (e.getStatusCode() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
//				// Create the topic if it doesn't exist
//				client.projects().topics()
//					.create(fullName, new Topic())
//					.execute();
//			} else {
//				throw e;
//			}
//		}
//	}
//
//	/**
//	 * Creates a Cloud Pub/Sub subscription if it doesn't exist.
//	 *
//	 * @param client Pubsub client object.
//	 * @throws IOException when API calls to Cloud Pub/Sub fails.
//	 */
//	private void setupSubscription(final Pubsub client) throws IOException {
//		String fullName = String.format("projects/%s/subscriptions/%s",
//			PubsubUtils.getProjectId(),
//			PubsubUtils.getAppSubscriptionName());
//
//		try {
//			client.projects().subscriptions().get(fullName).execute();
//		} catch (GoogleJsonResponseException e) {
//			if (e.getStatusCode() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
//				// Create the subscription if it doesn't exist
//				String fullTopicName = String.format("projects/%s/topics/%s",
//					PubsubUtils.getProjectId(),
//					PubsubUtils.getAppTopicName());
//				PushConfig pushConfig = new PushConfig()
//					.setPushEndpoint(PubsubUtils.getAppEndpointUrl());
//				Subscription subscription = new Subscription()
//					.setTopic(fullTopicName)
//					.setPushConfig(pushConfig);
//				client.projects().subscriptions()
//					.create(fullName, subscription)
//					.execute();
//			} else {
//				throw e;
//			}
//		}
//	}
}
