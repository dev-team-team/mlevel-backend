package com.mlevel.backend.service;

import com.google.api.client.util.Preconditions;
import com.google.appengine.repackaged.com.google.common.base.Pair;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.service.elastic.dto.common.Hit;
import com.mlevel.backend.service.elastic.dto.common.SearchResponse;
import com.mlevel.backend.service.elastic.dto.UserIndexDTO;
import com.mlevel.backend.service.elastic.rest.ElasticUserRestClient;
import com.mlevel.backend.service.model.Profile;
import com.mlevel.backend.service.model.User;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.List;

import static com.mlevel.backend.service.OfyService.ofy;

@Singleton
public class SearchService {

	@Inject
	private ProfileService profileService;

	@Inject
	private ElasticUserRestClient userRestClient;

	public Pair<Integer, List<UserIndexDTO>> findNearUsersByProfile(String userId, int from){
		Preconditions.checkNotNull(userId);
		final User user = ofy().load().type(User.class).id(Long.valueOf(userId)).now();
		user.getLocation();
		final Profile profile = profileService.getUserProfile(userId);
		DateTime dateTime = DateTime.parse("2016-02-02T20:54:00Z", ISODateTimeFormat.dateTimeNoMillis());

		SearchResponse<UserIndexDTO> response =  userRestClient.searchNearUsersByProfile(dateTime.toDate(), "200km", user, profile, from, 30);
		List<UserIndexDTO> searchResult = new ArrayList<>();
		for(Hit<UserIndexDTO> hit : response.hits.hits){
			searchResult.add(hit.source);
		}
		return  Pair.of(response.hits.total, searchResult);
	}
}
