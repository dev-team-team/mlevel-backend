package com.mlevel.backend.service;

import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.mlevel.backend.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.backend.service.model.User;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;
import com.mlevel.backend.service.model.authentication.FacebookUserInfo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static com.mlevel.backend.service.OfyService.ofy;

@Singleton
public class TestService {

	public AuthenticatedUserTokenDTO insertTestUser(){
		User user = new User();
		user.setFacebookUserInfo(new FacebookUserInfo("123123123", "fasdfasf213412341234"));
		DateFormat format = new SimpleDateFormat("MM/dd/YYYY");
		try {
			user.setBirthday(format.parse("03/02/1990"));
			user.setEmail("agustinsarasua@gmail.com");
			user.setLastName("Sarasua");
			user.setName("Agustin");
			user.setVerified(true);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Key<User> userKey = ofy().save().entity(user).now();
		AuthorizationToken authorizationToken = new AuthorizationToken(userKey, 2592000);
		authorizationToken.setToken("test-token");
		Key<AuthorizationToken> authToken = ofy().save().entity(authorizationToken).now();
		user.setAuthorizationToken(Ref.create(authToken));
		ofy().save().entity(user).now();
		return new AuthenticatedUserTokenDTO(user.getId(), authorizationToken.getToken());
	}
}
