package com.mlevel.backend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Preconditions;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.repackaged.com.google.common.base.Pair;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Work;
import com.mlevel.backend.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.backend.api.authentication.request.OAuth2RequestDTO;
import com.mlevel.backend.api.location.LocationDTO;
import com.mlevel.backend.service.elastic.dto.ElasticLocationDTO;
import com.mlevel.backend.service.elastic.dto.UserIndexDTO;
import com.mlevel.backend.service.model.Location;
import com.mlevel.backend.service.model.Profile;
import com.mlevel.backend.service.model.User;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;
import com.mlevel.backend.service.model.authentication.FacebookUserInfo;
import com.mlevel.backend.service.rest.dto.DebugTokenDTO;
import com.mlevel.backend.service.rest.dto.PublicProfileDTO;
import com.mlevel.backend.service.util.Constants;
import org.joda.time.DateTime;
import org.joda.time.Years;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import static com.mlevel.backend.service.OfyService.ofy;

@Singleton
public class UserService {

	private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());

	@Inject
	private FacebookService facebookService;

	@Inject
	private ProfileService profileService;

	public AuthenticatedUserTokenDTO socialLogin(final OAuth2RequestDTO request) {
		Preconditions.checkNotNull(request);
		Preconditions.checkNotNull(request.getAccessToken());
		final DebugTokenDTO debugTokenDTO = facebookService.debugAccessToken(request.getAccessToken());
		if(debugTokenDTO.getData().isValid){
			final User user = ofy().load().type(User.class).filter("facebookUserInfo.id", debugTokenDTO.getData().getUserId()).first().now();
			if(user != null){
				AuthorizationToken authoToken2 = ofy().transact(new Work<AuthorizationToken>() {

					@Override
					public AuthorizationToken run() {
						AuthorizationToken authorizationToken = user.getAuthorizationToken().getValue();
						ofy().delete().entity(authorizationToken).now();
						AuthorizationToken newAuthToken = new AuthorizationToken(Key.create(user), 2592000);
						Key<AuthorizationToken> newKey = ofy().save().entity(newAuthToken).now();
						user.setAuthorizationToken(Ref.create(newKey));
						ofy().save().entity(user).now();
						return newAuthToken;
					}
				});
				return new AuthenticatedUserTokenDTO(user.getId(), authoToken2.getToken());
			} else {
				final PublicProfileDTO publicProfileDTO = facebookService.getUserPublicProfile(debugTokenDTO.getData().getUserId());
				User userNew = ofy().transact(new Work<User>() {
					public User run() {
						User user = new User();
						user.setFacebookUserInfo(new FacebookUserInfo(debugTokenDTO.getData().getUserId(), request.getAccessToken()));
						DateFormat format = new SimpleDateFormat("MM/dd/YYYY");
						try {
							if(publicProfileDTO.birthday != null) {
								user.setBirthday(format.parse(publicProfileDTO.birthday));
							}
							user.setEmail(publicProfileDTO.email);
							user.setLastName(publicProfileDTO.lastName);
							user.setName(publicProfileDTO.name);
							user.setVerified(true);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						Key<User> userKey = ofy().save().entity(user).now();
						AuthorizationToken authorizationToken = new AuthorizationToken(userKey, 2592000);
						Key<AuthorizationToken> authToken = ofy().save().entity(authorizationToken).now();
						user.setAuthorizationToken(Ref.create(authToken));
						ofy().save().entity(user).now();
						return user;
					}
				});
				return new AuthenticatedUserTokenDTO(userNew.getId(), userNew.getAuthorizationToken().getValue().getToken());
			}
		}
		return null;
	}

	public void updateUserLocation(String userId, Location location) {
		LOGGER.info("Updating User Location for user = " + userId);
		Preconditions.checkNotNull(location);
		Preconditions.checkNotNull(location.getLatitude());
		Preconditions.checkNotNull(location.getLongitude());
		Profile profile = profileService.getUserProfile(userId);
		Preconditions.checkNotNull(profile);
		Preconditions.checkNotNull(profile.getSex());
		Preconditions.checkNotNull(profile.getDiscoveryPreferences());

		final User user = ofy().load().type(User.class).id(Long.valueOf(userId)).now();
		user.setLocation(location);
		ofy().save().entity(user).now();

		ObjectMapper objectMapper = new ObjectMapper();
		Queue userQueue = QueueFactory.getQueue(Constants.INDEX_USER_QUEUE);
		try {
			UserIndexDTO userIndexDTO = new UserIndexDTO();
			userIndexDTO.setLocation(new ElasticLocationDTO(location.getLatitude(), location.getLongitude()));
			userIndexDTO.setTimestamp(new Date());
			userIndexDTO.setName(user.getName());
			userIndexDTO.setLastName(user.getLastName());
			userIndexDTO.setNickName(profile.getNickName());
			userIndexDTO.setId(userId);
			int age = Years.yearsBetween(new DateTime(user.getBirthday()), new DateTime()).getYears();
			userIndexDTO.setAge(age);
			userIndexDTO.setSex(profile.getSex());
			LOGGER.info("Pushing to user queue index " + userId);
			userQueue.add(TaskOptions.Builder.withMethod(TaskOptions.Method.PULL)
				.payload(objectMapper.writeValueAsString(userIndexDTO)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
