package com.mlevel.backend.service.builder;

import com.mlevel.backend.service.model.User;

import java.util.Date;

/**
 * Created by agustin on 26/05/16.
 */
public class UserBuilder {

	private User result;

	public UserBuilder(Long id) {
		result = new User();
		result.setId(id);
	}

	public UserBuilder name(String name) {
		result.setName(name);
		return this;
	}

	public UserBuilder lastName(String lastName){
		result.setLastName(lastName);
		return this;
	}

	public UserBuilder birthday(Date birthday){
		result.setBirthday(birthday);
		return this;
	}

	public UserBuilder email(String email){
		result.setEmail(email);
		return this;
	}
	public User build() {
		return result;
	}

}
