package com.mlevel.backend.service.elastic.dto;

/**
 * Created by agustin on 22/05/16.
 */
public class ElasticLocationDTO {

	private double lon;

	private double lat;

	public ElasticLocationDTO(){}

	public ElasticLocationDTO(double lat, double lon){
		this.lat = lat;
		this.lon = lon;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}
}
