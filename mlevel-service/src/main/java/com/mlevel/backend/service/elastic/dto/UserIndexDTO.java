package com.mlevel.backend.service.elastic.dto;

import java.util.Date;

/**
 * Created by agustin on 22/05/16.
 */
public class UserIndexDTO {

	private String id;

	private Date timestamp;

	private String sex;

	private int age;

	private String name;

	private String lastName;

	private String nickName;

	private ElasticLocationDTO location;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public ElasticLocationDTO getLocation() {
		return location;
	}

	public void setLocation(ElasticLocationDTO location) {
		this.location = location;
	}
}
