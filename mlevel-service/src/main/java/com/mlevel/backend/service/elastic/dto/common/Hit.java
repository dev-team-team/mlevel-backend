package com.mlevel.backend.service.elastic.dto.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Hit<T> {

	@JsonProperty("_id")
	public Long id;

	@JsonProperty("_source")
	public T source;
}
