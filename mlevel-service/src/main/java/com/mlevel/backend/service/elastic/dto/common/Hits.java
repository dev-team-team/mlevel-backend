package com.mlevel.backend.service.elastic.dto.common;

import java.util.List;

public class Hits<T> {
	public int total;
	public double maxScore;
	public List<Hit<T>> hits;
}
