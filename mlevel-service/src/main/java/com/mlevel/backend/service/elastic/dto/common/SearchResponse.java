package com.mlevel.backend.service.elastic.dto.common;

/**
 * Created by agustin on 24/05/16.
 */
public class SearchResponse<T>{
	public Long took;
	public boolean timedOut;
	public Hits<T> hits;
}
