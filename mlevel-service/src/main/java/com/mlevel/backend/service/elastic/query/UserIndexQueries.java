package com.mlevel.backend.service.elastic.query;

import java.util.List;

/**
 * Created by agustin on 24/05/16.
 */
public abstract class UserIndexQueries {

	public static final String FILTER_SEX = "{\"match\" : { \"sex\" : \"%s\" }},";
	public static final String FILTER_DISCOVERY = "{\"match\" : { \"discovery\" : \"%s\" }},";
	public static final String FILTER_AGE = "{\"range\": { \"age\": { \"gte\": \"%s\", \"lte\": \"%s\" }}},";
	public static final String FILTER_TIMESTAMP = "{\"range\": { \"timestamp\": { \"gte\": \"%s\" }}},";
	public static final String FILTER_LOCATION = "{\"geo_distance\" : {\"distance\" : \"%s\",\"location\" : {\"lat\":  %s,\"lon\": %s}}}";

//	public static final String SEARCH_NEAR_USERS_BY_PROFILE_SEX = "{\"query\":{\"bool\":{\"filter\":[" +
//		FILTER_SEX + FILTER_DISCOVERY + FILTER_AGE + FILTER_TIMESTAMP + FILTER_LOCATION +
//		"]}}}";
//	public static final String SEARCH_NEAR_USERS_BY_PROFILE_NO_SEX = "{\"query\":{\"bool\":{\"filter\":[" +
//		FILTER_DISCOVERY + FILTER_AGE + FILTER_TIMESTAMP + FILTER_LOCATION +
//		"]}}}";

	public static String getSearchNearUsersByProfileQuery(List<String> filters){
		StringBuilder sbQuery = new StringBuilder("{\"query\":{\"bool\":{\"filter\":[");
		for (String filter: filters) {
			sbQuery.append(filter);
		}
		sbQuery.append("]}}}");
		return sbQuery.toString();
	}
}
