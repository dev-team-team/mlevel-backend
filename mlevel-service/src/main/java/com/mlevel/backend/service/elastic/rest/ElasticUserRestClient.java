package com.mlevel.backend.service.elastic.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Preconditions;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.TaskHandle;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.service.elastic.dto.*;
import com.mlevel.backend.service.elastic.dto.common.BulkIndexResponse;
import com.mlevel.backend.service.elastic.dto.common.SearchResponse;
import com.mlevel.backend.service.elastic.query.UserIndexQueries;
import com.mlevel.backend.service.http.HttpClient;
import com.mlevel.backend.service.model.Profile;
import com.mlevel.backend.service.model.User;
import com.mlevel.backend.service.util.Constants;
import org.joda.time.DateTime;
import org.joda.time.Years;

import javax.servlet.ServletException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Singleton
public class ElasticUserRestClient {

	private static final Logger LOGGER = Logger.getLogger(ElasticUserRestClient.class.getName());

	@Inject
	private HttpClient httpClient;

	private static final String TYPE_NAME = "user";

	private static final String HOST = "http://elastic-00.c.pimba-backend.internal:9200/";

	private static final String INSERT_INDEX_URL = "/{}";

	private static final String SEARCH_POST_INDEX_URL = Constants.ELASTIC_PUBLIC_INDEX_NAME + "/" + TYPE_NAME + "/_search?from={}&size={}";

	private static final String BULK_INDEX_URL = Constants.ELASTIC_PUBLIC_INDEX_NAME + "/" + TYPE_NAME + "/_bulk";

	public IndexResponseDTO indexUser(User user, Profile profile){

		Preconditions.checkNotNull(profile);
		Preconditions.checkNotNull(user);
		Preconditions.checkNotNull(user.getLocation());
		UserIndexDTO userIndexDTO = new UserIndexDTO();
		userIndexDTO.setSex(profile.getSex());
		userIndexDTO.setName(user.getName());
		userIndexDTO.setLastName(user.getLastName());
		userIndexDTO.setNickName(profile.getNickName());
		int age = Years.yearsBetween(new DateTime(user.getBirthday()), new DateTime()).getYears();
		userIndexDTO.setAge(age);
		userIndexDTO.setTimestamp(new Date());
		userIndexDTO.setLocation(new ElasticLocationDTO(user.getLocation().getLatitude(), user.getLocation().getLongitude()));
		try {
			IndexResponseDTO responseDTO = httpClient.doPost(HOST + INSERT_INDEX_URL, userIndexDTO, new TypeReference<IndexResponseDTO>(){}, user.getId().toString());
			return responseDTO;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return null;
	}



	public void indexUsers(List<TaskHandle> userTasks, Queue usersQueue) {
		try {
			LOGGER.info("Indexing users...");
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setDateFormat(new SimpleDateFormat(Constants.DATE_TIME_FORMAT));
			StringBuilder req = new StringBuilder();
			for (TaskHandle task : userTasks){
				UserIndexDTO userIndexDTO = objectMapper.readValue(task.getPayload(), UserIndexDTO.class);
				String indexInfo = "{ \"index\":  { \"_id\" : \"" + userIndexDTO.getId() + "\" }}";
				req.append(indexInfo).append("\n");
				String obj = objectMapper.writeValueAsString(userIndexDTO);
				req.append(obj).append("\n");
			}
			LOGGER.info("Indexing String = " + req.toString());
			BulkIndexResponse responseDTO = httpClient.doPost(HOST + BULK_INDEX_URL, req.toString(), new TypeReference<BulkIndexResponse>(){});
			LOGGER.info("Users indexed");
			List<Boolean> deletedTasks = usersQueue.deleteTask(userTasks);
			for (Boolean operationSucceed : deletedTasks){
				if(!operationSucceed){
					LOGGER.info("Event was not deleted");
				}
			}
		} catch (Exception e) {
			LOGGER.severe("Error Indexing Users" + e.toString());
			e.printStackTrace();
		}
	}

	public SearchResponse<UserIndexDTO> searchNearUsersByProfile(Date fromDate, String distance, User user, Profile profile, int from, int size){
		Preconditions.checkNotNull(distance);
		Preconditions.checkNotNull(user);
		Preconditions.checkNotNull(profile);
		Preconditions.checkNotNull(fromDate);

		try {
			List<String> filters = new ArrayList<>();
			if(!"FM".equals(profile.getDiscoveryPreferences().getSex())){
				filters.add(String.format(UserIndexQueries.FILTER_SEX, profile.getDiscoveryPreferences().getSex()));
			}
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
			String dateFrom = sdf.format(fromDate);
			filters.add(String.format(UserIndexQueries.FILTER_DISCOVERY, true));
			filters.add(String.format(UserIndexQueries.FILTER_AGE, profile.getDiscoveryPreferences().getMinAge(), profile.getDiscoveryPreferences().getMaxAge()));
			filters.add(String.format(UserIndexQueries.FILTER_TIMESTAMP, dateFrom));
			filters.add(String.format(UserIndexQueries.FILTER_LOCATION, distance, user.getLocation().getLatitude(), user.getLocation().getLongitude()));
			LOGGER.info("Searching near users by profile...");
			String query = UserIndexQueries.getSearchNearUsersByProfileQuery(filters);
			SearchResponse<UserIndexDTO> responseDTO = httpClient.doPost(HOST + SEARCH_POST_INDEX_URL, query, new TypeReference<SearchResponse<UserIndexDTO>>(){}, String.valueOf(from), String.valueOf(size));
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
