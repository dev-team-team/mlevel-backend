package com.mlevel.backend.service.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.inject.Singleton;

import javax.servlet.ServletException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

@Singleton
public class HttpClient {

	public Object doGet(String uri, Class clazz, String... params) throws IOException, ServletException {

		HttpURLConnection conn = null;
		InputStream is = null;
		Object result = null;
		try {
			//constants
			URL url = new URL(this.buildUrl(uri, params));

			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /*milliseconds*/);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			//make some HTTP header nicety
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

			//open
			conn.connect();

			is = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));

			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			result = objectMapper.readValue(sb.toString(), clazz);
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			conn.disconnect();
		}
	}

	public <R> R  doPost(String uri, Object body, TypeReference<R> typeRef, String... params)
		throws IOException, ServletException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String message = objectMapper.writeValueAsString(body);
		return this.execute("POST", uri, message, typeRef, params);
	}

	public <R> R doPost(String uri, String body, TypeReference<R> typeRef, String... params)
		throws IOException, ServletException {

		return this.execute("POST", uri, body,typeRef, params);
	}

	private <R> R  execute(String method, String uri, String body, TypeReference<R> typeRef, String... params)
		throws IOException, ServletException {
		// [START complex]
		URL url = new URL(this.buildUrl(uri, params));
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(method);
		//make some HTTP header nicety
		conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
		writer.write(body);
		writer.close();

		int respCode = conn.getResponseCode();
		if (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NOT_FOUND) {
			StringBuffer response = new StringBuffer();
			String line;

			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();
			return objectMapper.readValue(response.toString(), typeRef);
		} else {
		}
		return null;
	}

	private String buildUrl(String serviceUrl, Object[] uriParams) {
		String urlSuffix = null;
		if(uriParams != null && uriParams.length > 0) {
			String partialUrl = serviceUrl.replaceAll("\\{\\}", "%s");
			urlSuffix = String.format(partialUrl, uriParams);
		} else {
			urlSuffix = serviceUrl;
		}
		return urlSuffix;
	}
}
