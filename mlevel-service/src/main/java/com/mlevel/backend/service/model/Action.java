package com.mlevel.backend.service.model;

import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfNull;

public class Action {

    private String name;

    @Unindex(IfNull.class)
    private String icon;

    public Action() {
    }

    public Action(String name, String description) {
        this.name = name;
        this.icon = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
