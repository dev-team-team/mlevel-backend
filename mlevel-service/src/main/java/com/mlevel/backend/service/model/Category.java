package com.mlevel.backend.service.model;

/**
 * Created by agustin on 19/05/16.
 */
public class Category {

	private String name;

	public Category(){}

	public Category(String name){ this.name = name;}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
