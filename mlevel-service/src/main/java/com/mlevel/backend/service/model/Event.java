package com.mlevel.backend.service.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Subclass;
import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfNull;
import com.mlevel.backend.api.timeline.TimelineItemDTO;

import java.util.List;

@Subclass(index=true)
public class Event extends TimelineItem {

	@Unindex(IfNull.class)
	private String facebookId;

	@Load
	private Ref<Cover> cover;

	private String eventType;

	private List<Action> commonActions;

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Ref<Cover> getCover() {
		return cover;
	}

	public void setCover(Ref<Cover> cover) {
		this.cover = cover;
	}

	public List<Action> getCommonActions() {
		return commonActions;
	}

	public void setCommonActions(List<Action> commonActions) {
		this.commonActions = commonActions;
	}

	@Override
	public TimelineItemDTO.TimelineItemType getType() {
		return null;
	}
}
