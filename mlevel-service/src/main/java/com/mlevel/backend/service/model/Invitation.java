package com.mlevel.backend.service.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.*;
import com.googlecode.objectify.condition.IfNull;

import java.util.Date;

@Entity
public class Invitation {

	public enum State { ACTIVE, ACCEPTED, DELETED,  CLOSED }

	@Id
	private Long id;

	@Index
	@Load
	private Ref<User> sender;

	@Index
	@Load
	private Ref<User> receiver;

	private Date timestamp;

	private Date startTime;

	private Date endTime;

	@Unindex(IfNull.class)
	@Load
	private Ref<Event> event;

	private Action action;

	private State state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ref<User> getSender() {
		return sender;
	}

	public void setSender(Ref<User> sender) {
		this.sender = sender;
	}

	public Ref<User> getReceiver() {
		return receiver;
	}

	public void setReceiver(Ref<User> receiver) {
		this.receiver = receiver;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Ref<Event> getEvent() {
		return event;
	}

	public void setEvent(Ref<Event> event) {
		this.event = event;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
