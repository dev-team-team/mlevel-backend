package com.mlevel.backend.service.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfNull;
import com.mlevel.backend.api.profile.ImageDTO;

import java.util.List;

@Entity
public class Profile {

    @Id
    private Long id;

    private String nickName;

    private String shortMessage;

    private List<Category> categories;

    private DiscoveryPreferences discoveryPreferences;

    @Index
    private Ref<User> user;

    private String sex;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Ref<User> getUser() {
        return user;
    }

    public void setUser(Ref<User> user) {
        this.user = user;
    }

    public DiscoveryPreferences getDiscoveryPreferences() {
        return discoveryPreferences;
    }

    public void setDiscoveryPreferences(DiscoveryPreferences discoveryPreferences) {
        this.discoveryPreferences = discoveryPreferences;
    }
}
