package com.mlevel.backend.service.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfNull;
import com.mlevel.backend.api.timeline.TimelineItemDTO;

import java.util.Date;

@Entity
public abstract class TimelineItem {

	@Id
	private Long id;

	private String name;

	@Unindex(IfNull.class)
	private String subtitle;

	private String description;

	@Load
	private Ref<Place> place;

	private Date startTime;

	private Date endTime;

	private Date creationDate;

	public abstract TimelineItemDTO.TimelineItemType getType();

	public enum TimelineItemType {
		EVENT, OPEN_INVITATION
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Ref<Place> getPlace() {
		return place;
	}

	public void setPlace(Ref<Place> place) {
		this.place = place;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
