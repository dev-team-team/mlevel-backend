package com.mlevel.backend.service.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfNull;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;
import com.mlevel.backend.service.model.authentication.FacebookUserInfo;

import java.util.*;

@Entity
public class User {

    @Id
    private Long id;

    @Unindex(IfNull.class)
    private String name;

    @Unindex(IfNull.class)
    private String lastName;

    @Unindex(IfNull.class)
    private FacebookUserInfo facebookUserInfo;

    @Unindex(IfNull.class)
    private Date birthday;

    @Unindex(IfNull.class)
    private String email;

    @Unindex(IfNull.class)
    private String hashedPassword;

    private boolean isVerified;

    @Unindex(IfNull.class)
    private Ref<Profile> profile;

    @Load
    private Ref<AuthorizationToken> authorizationToken;

    private Location location;

    public FacebookUserInfo getFacebookUserInfo() {
        return facebookUserInfo;
    }

    public void setFacebookUserInfo(FacebookUserInfo facebookUserInfo) {
        this.facebookUserInfo = facebookUserInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public Ref<AuthorizationToken> getAuthorizationToken() {
        return authorizationToken;
    }

    public void setAuthorizationToken(Ref<AuthorizationToken> authorizationToken) {
        this.authorizationToken = authorizationToken;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ref<Profile> getProfile() {
        return profile;
    }

    public void setProfile(Ref<Profile> profile) {
        this.profile = profile;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
