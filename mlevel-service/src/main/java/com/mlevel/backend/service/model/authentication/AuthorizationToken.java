package com.mlevel.backend.service.model.authentication;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.mlevel.backend.service.model.User;

import java.util.Date;
import java.util.UUID;

@Entity
public class AuthorizationToken {

    @Id
    private Long id;

    private final static Integer DEFAULT_TIME_TO_LIVE_IN_SECONDS = (60 * 60 * 24 * 30); //30 Days

    @Index
    private String token;

    private Date expirationDate;

    @Load
    private Ref<User> user;

    public AuthorizationToken() {}

    public AuthorizationToken(Key<User> userKey) {
        this(userKey, DEFAULT_TIME_TO_LIVE_IN_SECONDS);
    }

    public AuthorizationToken(Key<User> userKey, Integer timeToLiveInSeconds) {
        this.token = UUID.randomUUID().toString();
        this.user = Ref.create(userKey);
        this.expirationDate = new Date(System.currentTimeMillis() + (timeToLiveInSeconds * 1000L));
    }

    public boolean hasExpired() {
        return this.expirationDate != null && this.expirationDate.before(new Date());
    }

    public String getToken() {
        return token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Ref<User> getUser() {
        return user;
    }

    public void setUser(Ref<User> user) {
        this.user = user;
    }
}
