package com.mlevel.backend.service.model.authentication;


import com.googlecode.objectify.annotation.Index;

@Index
public class FacebookUserInfo {

	private String id;

	private String accessToken;

	public FacebookUserInfo(){}

	public FacebookUserInfo(String id, String accessToken){
		this.id = id;
		this.accessToken = accessToken;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
