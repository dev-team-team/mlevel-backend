package com.mlevel.backend.service.model.authentication;

public enum Role {
    AUTHENTICATED, ADMINISTRATOR, ANONYMOUS
}
