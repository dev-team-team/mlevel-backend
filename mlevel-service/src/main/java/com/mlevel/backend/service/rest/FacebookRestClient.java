package com.mlevel.backend.service.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.service.http.HttpClient;
import com.mlevel.backend.service.rest.dto.DebugTokenDTO;
import com.mlevel.backend.service.rest.dto.PublicProfileDTO;

import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URLEncoder;

@Singleton
public class FacebookRestClient {

	@Inject
	private HttpClient httpClient;

	public static final String HOST = "https://graph.facebook.com/v2.6";

	private static final String APP_ACCESS_TOKEN = "1709613959262846|_XCZ7ieEvNPHE4sEJTOK659iFDY";

	public static final String DEBUG_TOKEN_URL = "/debug_token?input_token={}&access_token={}";

	public static final String USER_PROFILE_URL = "/{}?fields=last_name,birthday,cover,email,name&access_token={}";

	public DebugTokenDTO debugUserAccessToken(String accessToken){
		try {
			DebugTokenDTO debugTokenDTO = (DebugTokenDTO)
				httpClient.doGet(HOST + DEBUG_TOKEN_URL, DebugTokenDTO.class,
					URLEncoder.encode(accessToken, "UTF-8"),
					URLEncoder.encode(APP_ACCESS_TOKEN, "UTF-8"));
			return debugTokenDTO;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public PublicProfileDTO getUserPublicProfile(String userId){
		try {
			PublicProfileDTO publicProfileDTO = (PublicProfileDTO)
				httpClient.doGet(HOST + USER_PROFILE_URL, PublicProfileDTO.class,
					userId,
					URLEncoder.encode(APP_ACCESS_TOKEN, "UTF-8"));
			return publicProfileDTO;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

}
