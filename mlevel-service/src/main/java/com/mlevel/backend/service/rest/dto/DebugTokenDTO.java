package com.mlevel.backend.service.rest.dto;

/**
 * Created by agustin on 15/05/16.
 */
public class DebugTokenDTO {

	private Data data;

	public class Data {

		private String appId;

		//Unixtime
		private long expiresAt;

		public boolean isValid;

		private String userId;

		public String getAppId() {
			return appId;
		}

		public void setAppId(String appId) {
			this.appId = appId;
		}

		public long getExpiresAt() {
			return expiresAt;
		}

		public void setExpiresAt(long expiresAt) {
			this.expiresAt = expiresAt;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
