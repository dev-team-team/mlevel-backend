package com.mlevel.backend.service.util;

/**
 * Created by agustin on 28/04/16.
 */
public class Constants {

	public static final String ELASTIC_PUBLIC_INDEX_NAME = "public";

	public static final String INDEX_EVENT_QUEUE = "index-event-queue";
	public static final String INDEX_USER_QUEUE = "index-user-queue";

	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

	public static final String APP_ACCESS_TOKEN = "1709613959262846|_XCZ7ieEvNPHE4sEJTOK659iFDY";

	//Client Secret B9qr5bZ6hQO0gz_uMHRYNV0A
	public static final String WEB_CLIENT_ID = "984484728378-ocm7q8qfn8e1nimv1ge7v2uj1t453bd8.apps.googleusercontent.com";
	public static final String ANDROID_CLIENT_ID = "${android-client-id}";
	public static final String IOS_CLIENT_ID = "${ios-client-id}";
	public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

	public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";


//	/**
//	 * A memcache key for storing query result for recent messages.
//	 */
//	public static final String MESSAGE_CACHE_KEY = "messageCache";
//	/**
//	 * A base package name.
//	 */
//	public static final String BASE_PACKAGE =
//		"com.mlevel";

}
