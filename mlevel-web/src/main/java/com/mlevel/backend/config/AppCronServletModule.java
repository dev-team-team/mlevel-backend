package com.mlevel.backend.config;

import com.google.inject.servlet.ServletModule;
import com.mlevel.backend.cron.IndexEventsCron;
import com.mlevel.backend.cron.IndexUserCron;
import com.mlevel.backend.servlet.ImageUploadedServlet;

/**
 * Created by agustin on 15/05/16.
 */
public class AppCronServletModule extends ServletModule {

	@Override
	protected void configureServlets() {
		serve("/cron/indexUsers").with(IndexUserCron.class);
		serve("/cron/indexEvents").with(IndexEventsCron.class);
		serve("/image/upload").with(ImageUploadedServlet.class);
	}
}
