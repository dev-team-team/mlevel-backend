package com.mlevel.backend.config;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * Created by agustin on 12/05/16.
 */
public class AppGuiceServletContextListener extends GuiceServletContextListener {

	@Override
	protected Injector getInjector() {
		return Guice.createInjector(
			new AppGuiceSystemServiceServletModule(),
			new AppInjector(),
			new AppCronServletModule());
	}
}
