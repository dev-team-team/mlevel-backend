package com.mlevel.backend.config;

import com.google.api.server.spi.guice.GuiceSystemServiceServletModule;
import com.googlecode.objectify.ObjectifyFilter;
import com.mlevel.backend.endpoint.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by agustin on 12/05/16.
 */
public class AppGuiceSystemServiceServletModule extends GuiceSystemServiceServletModule {

	@Override
	protected void configureServlets() {
		super.configureServlets();
		filter("/*").through(ObjectifyFilter.class);

		Set<Class<?>> serviceClasses = new HashSet<>();
		serviceClasses.add(EventEndpoint.class);
		serviceClasses.add(BlobUrlEndpoint.class);
		serviceClasses.add(UserEndpoint.class);
		serviceClasses.add(ProfileEndpoint.class);
		serviceClasses.add(SearchEndpoint.class);
		serviceClasses.add(TestEndpoint.class);
		this.serveGuiceSystemServiceServlet("/_ah/spi/*", serviceClasses);
	}

}
