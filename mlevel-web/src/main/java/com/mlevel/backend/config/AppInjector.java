package com.mlevel.backend.config;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyFilter;
import com.mlevel.backend.security.CustomAuthenticator;

/**
 * Created by agustin on 12/05/16.
 */
public class AppInjector extends AbstractModule {
	@Override
	protected void configure() {
		bind(ObjectifyFilter.class).in(Singleton.class);
	}

}
