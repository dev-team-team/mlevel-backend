package com.mlevel.backend.cron;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskHandle;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.service.EventService;
import com.mlevel.backend.service.util.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Singleton
public class IndexEventsCron extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(IndexEventsCron.class.getName());

	private final int numberOfTasksToLease = 100;

	@Inject
	private EventService eventService;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		LOGGER.info("Running IndexEvents Cron...");
		Queue eventsQueue = QueueFactory.getQueue(Constants.INDEX_EVENT_QUEUE);
		List<TaskHandle> tasks = eventsQueue.leaseTasks(3600, TimeUnit.SECONDS, numberOfTasksToLease);
		eventService.indexEvents(tasks, eventsQueue);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		doGet(req, resp);
	}
}
