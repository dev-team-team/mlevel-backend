package com.mlevel.backend.cron;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskHandle;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.service.EventService;
import com.mlevel.backend.service.elastic.rest.ElasticUserRestClient;
import com.mlevel.backend.service.util.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by agustin on 14/05/16.
 */
@Singleton
public class IndexUserCron extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(IndexUserCron.class.getName());

	private final int numberOfTasksToLease = 100;

	@Inject
	private ElasticUserRestClient userRestClient;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		LOGGER.info("Running IndexUser Cron...");
		Queue usersQueue = QueueFactory.getQueue(Constants.INDEX_USER_QUEUE);
		List<TaskHandle> tasks = usersQueue.leaseTasks(3600, TimeUnit.SECONDS, numberOfTasksToLease);
		userRestClient.indexUsers(tasks, usersQueue);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		doGet(req, resp);
	}
}
