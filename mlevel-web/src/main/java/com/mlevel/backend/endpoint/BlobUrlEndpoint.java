package com.mlevel.backend.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.AuthLevel;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.mlevel.backend.api.SimpleResponseDTO;
import com.mlevel.backend.api.blob.BlobUrlDTO;
import com.mlevel.backend.api.blob.ImageUploadedDTO;
import com.mlevel.backend.security.CustomAuthenticator;

@Api(name = "blob",
	version = "v1",
	authenticators = {CustomAuthenticator.class})
public class BlobUrlEndpoint {

	BlobstoreService blobService = BlobstoreServiceFactory.getBlobstoreService();

	@ApiMethod(name="url",
		path = "url",
		httpMethod = ApiMethod.HttpMethod.GET,
		authenticators = {CustomAuthenticator.class})
	public BlobUrlDTO getImageBlobUrl(User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		/*
		This is where the client will be redirected once the actual upload has been completed.
		That's where you'll handle storing the blobkey and/or serving url and returning it to the client.
		 */
		String blobUploadUrl = blobService.createUploadUrl("/image/upload");
		return new BlobUrlDTO(blobUploadUrl);
	}

	@ApiMethod(name="image.uploaded",
		path = "image/uploaded",
		httpMethod = ApiMethod.HttpMethod.POST,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO saveImageUploaded(ImageUploadedDTO imageUploadedDTO, User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		// TODO save user image reference in datastore

		return new SimpleResponseDTO(SimpleResponseDTO.CommonResult.OK.toString());
	}
}
