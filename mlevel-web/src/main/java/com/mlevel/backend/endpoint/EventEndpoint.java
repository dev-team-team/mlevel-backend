package com.mlevel.backend.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.inject.Inject;
import com.mlevel.backend.api.EventDTO;
import com.mlevel.backend.api.authentication.UserDTO;
import com.mlevel.backend.dto.SimpleResponseDTO;
import com.mlevel.backend.mapper.EventMapper;
import com.mlevel.backend.security.CustomAuthenticator;
import com.mlevel.backend.service.EventService;
import com.mlevel.backend.service.model.Event;
import com.mlevel.backend.service.util.Constants;

@Api(name = "event",
	version = "v1",
	authenticators = {CustomAuthenticator.class})
public class EventEndpoint {

	@Inject
	private EventService eventService;

	@Inject
	private EventMapper eventMapper;

	@ApiMethod(name = "events.add", httpMethod = ApiMethod.HttpMethod.POST)
	public EventDTO addEvent(User user, EventDTO eventDTO) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		Event event = eventMapper.mapToEntity(eventDTO);
		eventService.createEvent(event);
		return eventDTO;
	}

}
