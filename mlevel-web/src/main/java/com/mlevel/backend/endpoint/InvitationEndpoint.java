package com.mlevel.backend.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.inject.Inject;
import com.mlevel.backend.api.InvitationDTO;
import com.mlevel.backend.api.SimpleResponseDTO;
import com.mlevel.backend.mapper.InvitationMapper;
import com.mlevel.backend.security.CustomAuthenticator;
import com.mlevel.backend.service.InvitationService;
import com.mlevel.backend.service.model.Invitation;

@Api(name = "invitation",
	version = "v1")
public class InvitationEndpoint {

	@Inject
	private InvitationMapper invitationMapper;

	@Inject
	private InvitationService invitationService;

	@ApiMethod(name="post.invitation",
		path = "create",
		httpMethod = ApiMethod.HttpMethod.POST,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO createInvitation(InvitationDTO invitationDTO, User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		invitationDTO.senderId = Long.valueOf(user.getId());
		Invitation invitation = invitationMapper.mapToEntity(invitationDTO);
		invitation = invitationService.createInvitation(invitation);
		return new SimpleResponseDTO(SimpleResponseDTO.CommonResult.OK.name(), invitation.getId().toString());
	}

	@ApiMethod(name="put.invitation.accept",
		path = "accept/{invitationId}",
		httpMethod = ApiMethod.HttpMethod.PUT,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO acceptInvitation(@Named("invitationId") Long invitationId, User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		boolean result = invitationService.updateInvitationState(invitationId, Invitation.State.ACCEPTED);
		return new SimpleResponseDTO(result ? SimpleResponseDTO.CommonResult.OK.name() : SimpleResponseDTO.CommonResult.ERROR.name());
	}

	@ApiMethod(name="put.invitation.reject",
		path = "reject/{invitationId}",
		httpMethod = ApiMethod.HttpMethod.PUT,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO rejectInvitation(@Named("invitationId") Long invitationId, User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		boolean result = invitationService.updateInvitationState(invitationId, Invitation.State.DELETED);
		return new SimpleResponseDTO(result ? SimpleResponseDTO.CommonResult.OK.name() : SimpleResponseDTO.CommonResult.ERROR.name());
	}

}
