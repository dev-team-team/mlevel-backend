package com.mlevel.backend.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.AuthLevel;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.inject.Inject;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.mlevel.backend.api.SimpleResponseDTO;
import com.mlevel.backend.api.profile.ProfileDTO;
import com.mlevel.backend.mapper.ProfileMapper;
import com.mlevel.backend.security.CustomAuthenticator;
import com.mlevel.backend.service.ProfileService;
import com.mlevel.backend.service.model.Profile;

import java.util.logging.Logger;

@Api(name = "profile",
	version = "v1")
public class ProfileEndpoint {

	private static final Logger LOGGER = Logger.getLogger(UserEndpoint.class.getName());

	@Inject
	private ProfileMapper profileMapper;

	@Inject
	private ProfileService profileService;

	@ApiMethod(name="get.profile",
		httpMethod = ApiMethod.HttpMethod.GET,
		authenticators = {CustomAuthenticator.class})
	public ProfileDTO userProfile(User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		LOGGER.info("Get Profile for user = "+user.getId());
		Profile profile = profileService.getUserProfile(user.getId());
		return profileMapper.mapToDTO(profile);
	}

	@ApiMethod(name="post.profile.categories",
		path = "categories",
		httpMethod = ApiMethod.HttpMethod.POST,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO userCategories(ProfileDTO profileDTO, User user) throws UnauthorizedException{
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		LOGGER.info("Updating user categories, user = "+user.getId());
		Profile profile = profileMapper.mapToEntity(profileDTO);
		profile.setUser(Ref.create(Key.create(com.mlevel.backend.service.model.User.class, user.getId())));
		profile = profileService.updateUserCategories(profile);
		return new SimpleResponseDTO(SimpleResponseDTO.CommonResult.OK.name(), profile.getId().toString());
	}

	@ApiMethod(name="post.profile.preferences",
		path = "preferences",
		httpMethod = ApiMethod.HttpMethod.POST,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO userPreferences(ProfileDTO profileDTO, User user) throws UnauthorizedException{
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		LOGGER.info("Updating user preferences, user = "+user.getId());
		Profile profile = profileMapper.mapToEntity(profileDTO);
		profile.setUser(Ref.create(Key.create(com.mlevel.backend.service.model.User.class, user.getId())));
		profile = profileService.updateUserPreferences(profile);
		return new SimpleResponseDTO(SimpleResponseDTO.CommonResult.OK.name(), profile.getId().toString());
	}
}
