package com.mlevel.backend.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.AuthLevel;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.repackaged.com.google.common.base.Pair;
import com.google.inject.Inject;
import com.mlevel.backend.api.SearchResponseDTO;
import com.mlevel.backend.api.SimpleResponseDTO;
import com.mlevel.backend.security.CustomAuthenticator;
import com.mlevel.backend.service.SearchService;
import com.mlevel.backend.service.elastic.dto.UserIndexDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.logging.Logger;

@Api(name = "search",
	version = "v1")
public class SearchEndpoint {

	private static final Logger LOGGER = Logger.getLogger(SearchEndpoint.class.getName());

	@Inject
	private SearchService searchService;

	@ApiMethod(name="post.loadNearUsersByProfile",
		path = "near-users",
		httpMethod = ApiMethod.HttpMethod.GET,
		authenticators = {CustomAuthenticator.class})
	public SearchResponseDTO findNearUsersByProfile(@Named("from") String from, User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		int fromPage = 0;
		if(StringUtils.isNotEmpty(from)){
			fromPage = Integer.parseInt(from);
		}
		Pair<Integer, List<UserIndexDTO>> searchResult = searchService.findNearUsersByProfile(user.getId(), fromPage);
		return new SearchResponseDTO(searchResult.getSecond(), searchResult.getFirst());
	}
}
