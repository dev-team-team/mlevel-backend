package com.mlevel.backend.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.inject.Inject;
import com.mlevel.backend.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.backend.api.authentication.request.OAuth2RequestDTO;
import com.mlevel.backend.service.TestService;

@Api(name = "test",
	version = "v1")
public class TestEndpoint {

	@Inject
	private TestService testService;

	@ApiMethod(name = "test.login",
		path = "test/login",
		httpMethod = ApiMethod.HttpMethod.POST)
	public AuthenticatedUserTokenDTO testLogin(OAuth2RequestDTO request) {
		return testService.insertTestUser();
	}

}
