package com.mlevel.backend.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.repackaged.com.google.common.base.Pair;
import com.google.inject.Inject;
import com.mlevel.backend.api.SearchResponseDTO;
import com.mlevel.backend.api.SimpleResponseDTO;
import com.mlevel.backend.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.backend.api.authentication.request.OAuth2RequestDTO;
import com.mlevel.backend.api.location.LocationDTO;
import com.mlevel.backend.mapper.LocationMapper;
import com.mlevel.backend.security.CustomAuthenticator;
import com.mlevel.backend.service.InvitationService;
import com.mlevel.backend.service.UserService;
import com.mlevel.backend.service.model.Invitation;

import java.util.List;
import java.util.logging.Logger;

@Api(name = "user",
	version = "v1")
public class UserEndpoint {

	private static final Logger LOGGER = Logger.getLogger(UserEndpoint.class.getName());

	@Inject
	private UserService userService;

	@Inject
	private LocationMapper locationMapper;

	@Inject
	private InvitationService invitationService;

	@ApiMethod(name = "login.facebook",
		path = "login/facebook",
		httpMethod = ApiMethod.HttpMethod.POST)
	public AuthenticatedUserTokenDTO loginFacebook(OAuth2RequestDTO request) {
		return userService.socialLogin(request);
	}

	@ApiMethod(name="post.location",
		path = "place",
		httpMethod = ApiMethod.HttpMethod.POST,
		authenticators = {CustomAuthenticator.class})
	public SimpleResponseDTO updateUserLocation(LocationDTO locationDTO, User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		LOGGER.info("Updating user location, user = "+user.getId());
		userService.updateUserLocation(user.getId(), locationMapper.mapToEntity(locationDTO));
		return new SimpleResponseDTO(SimpleResponseDTO.CommonResult.OK.name());
	}

	@ApiMethod(name="get.invitations",
		path = "invitation",
		httpMethod = ApiMethod.HttpMethod.GET,
		authenticators = {CustomAuthenticator.class})
	public SearchResponseDTO listInvitations(User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		LOGGER.info("List invitations made to the user = "+ user.getId());
		Pair<Integer, List<Invitation>> result = invitationService.loadUserInvitations(Long.valueOf(user.getId()), 0);
		return new SearchResponseDTO(result.getSecond(), result.getFirst());
	}

	@ApiMethod(name="get.invites",
		path = "invites",
		httpMethod = ApiMethod.HttpMethod.GET,
		authenticators = {CustomAuthenticator.class})
	public SearchResponseDTO listInvitates(User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		LOGGER.info("List invitations made to the user = "+ user.getId());
		Pair<Integer, List<Invitation>> result = invitationService.loadUserInvites(Long.valueOf(user.getId()), 0);
		return new SearchResponseDTO(result.getSecond(), result.getFirst());
	}

}
