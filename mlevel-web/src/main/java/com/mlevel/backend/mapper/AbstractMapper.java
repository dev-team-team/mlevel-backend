package com.mlevel.backend.mapper;

import com.mlevel.backend.api.AbstractDTO;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMapper<T, DTO extends AbstractDTO> {

    public abstract T mapToEntity(DTO dto);

    public List<T> mapToEntityList(List<DTO> dtos){
        List<T> result = new ArrayList<>();
        for (DTO dto : dtos){
            result.add(mapToEntity(dto));
        }
        return result;
    }

    public abstract DTO mapToDTO(T entity);

    public List<DTO> mapToDTOList(List<T> entities){
        List<DTO> result = new ArrayList<>();
        for (T entity : entities){
            result.add(mapToDTO(entity));
        }
        return result;
    }
}
