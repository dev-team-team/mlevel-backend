package com.mlevel.backend.mapper;

import com.google.inject.Singleton;
import com.mlevel.backend.api.timeline.ActionDTO;
import com.mlevel.backend.service.model.Action;

@Singleton
public class ActionMapper extends AbstractMapper<Action, ActionDTO> {

    @Override
    public Action mapToEntity(ActionDTO dto) {
        return null;
    }

    @Override
    public ActionDTO mapToDTO(Action entity) {
        ActionDTO dto = new ActionDTO();
        dto.setName(entity.getName());
        dto.setIcon(entity.getIcon());
        return dto;
    }

}
