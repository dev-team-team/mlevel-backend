package com.mlevel.backend.mapper;

import com.mlevel.backend.api.profile.CategoryDTO;
import com.mlevel.backend.service.model.Category;

/**
 * Created by agustin on 20/05/16.
 */
public class CategoryMapper extends AbstractMapper<Category, CategoryDTO> {

	@Override
	public Category mapToEntity(CategoryDTO dto) {
		Category category = new Category();
		category.setName(dto.getName());
		return category;
	}

	@Override
	public CategoryDTO mapToDTO(Category entity) {
		CategoryDTO category = new CategoryDTO();
		category.setName(entity.getName());
		return category;
	}
}
