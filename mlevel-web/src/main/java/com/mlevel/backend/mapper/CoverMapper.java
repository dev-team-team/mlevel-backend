package com.mlevel.backend.mapper;

import com.google.inject.Singleton;
import com.mlevel.backend.api.timeline.CoverDTO;
import com.mlevel.backend.service.model.Cover;

@Singleton
public class CoverMapper extends AbstractMapper<Cover, CoverDTO> {

	@Override
	public Cover mapToEntity(CoverDTO dto) {
		Cover cover = new Cover();
		cover.setId(dto.getId());
		cover.setFacebookId(dto.getFacebookId());
		cover.setOffsetX(dto.getOffsetX());
		cover.setOffsetY(dto.getOffsetY());
		cover.setSource(dto.getSource());
		return cover;
	}

	@Override
	public CoverDTO mapToDTO(Cover entity) {
		CoverDTO cover = new CoverDTO();
		cover.setId(entity.getId());
		cover.setFacebookId(entity.getFacebookId());
		cover.setOffsetX(entity.getOffsetX());
		cover.setOffsetY(entity.getOffsetY());
		cover.setSource(entity.getSource());
		return cover;
	}
}
