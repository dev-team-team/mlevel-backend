package com.mlevel.backend.mapper;

import com.google.inject.Singleton;
import com.mlevel.backend.api.profile.DiscoveryPreferencesDTO;
import com.mlevel.backend.service.model.DiscoveryPreferences;

@Singleton
public class DiscoveryPreferencesMapper extends AbstractMapper<DiscoveryPreferences, DiscoveryPreferencesDTO> {

	@Override
	public DiscoveryPreferences mapToEntity(DiscoveryPreferencesDTO dto) {
		DiscoveryPreferences discoveryPreferences = new DiscoveryPreferences();
		discoveryPreferences.setDiscovery(dto.isDiscovery());
		discoveryPreferences.setFindable(dto.isFindable());
		discoveryPreferences.setMaxAge(dto.getMaxAge());
		discoveryPreferences.setMinAge(dto.getMinAge());
		discoveryPreferences.setSex(dto.getSex());
		return discoveryPreferences;
	}

	@Override
	public DiscoveryPreferencesDTO mapToDTO(DiscoveryPreferences entity) {
		DiscoveryPreferencesDTO discoveryPreferences = new DiscoveryPreferencesDTO();
		discoveryPreferences.setDiscovery(entity.isDiscovery());
		discoveryPreferences.setFindable(entity.isFindable());
		discoveryPreferences.setMaxAge(entity.getMaxAge());
		discoveryPreferences.setMinAge(entity.getMinAge());
		discoveryPreferences.setSex(entity.getSex());
		return discoveryPreferences;
	}
}
