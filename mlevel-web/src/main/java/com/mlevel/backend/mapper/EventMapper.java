package com.mlevel.backend.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.Ref;
import com.mlevel.backend.api.EventDTO;
import com.mlevel.backend.service.model.Event;

import java.util.Date;
import java.util.List;

@Singleton
public class EventMapper extends AbstractMapper<Event, EventDTO> {

    @Inject
    private ActionMapper actionMapper;

    @Inject
    private PlaceMapper placeMapper;

    @Inject
    private CoverMapper coverMapper;

    @Override
    public Event mapToEntity(EventDTO dto) {
        Event event = new Event();
        event.setName(dto.getName());
        event.setSubtitle(dto.getSubtitle());
        event.setCreationDate(dto.getCreationDate());
        event.setDescription(dto.getDescription());
        event.setEndTime(dto.getEndTime());
        event.setStartTime(dto.getStartTime());
        event.setEventType(dto.getEventType());
        if(dto.getPlaceDTO() != null) {
            event.setPlace(Ref.create(placeMapper.mapToEntity(dto.getPlaceDTO())));
        }
        if(dto.getCommonActions() != null) {
            event.setCommonActions(actionMapper.mapToEntityList(dto.getCommonActions()));
        }
        if(dto.getCoverDTO() != null){
            event.setCover(Ref.create(coverMapper.mapToEntity(dto.getCoverDTO())));
        }
        event.setFacebookId(dto.getFacebookId());
        event.setId(dto.getId());
        return event;
    }

    @Override
    public EventDTO mapToDTO(Event entity) {
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId(entity.getId());
        eventDTO.setName(entity.getName());
        eventDTO.setDescription(entity.getDescription());
        eventDTO.setCoverDTO(coverMapper.mapToDTO(entity.getCover().getValue()));
        eventDTO.setEventType(entity.getEventType());
        eventDTO.setCreationDate(entity.getCreationDate());
        eventDTO.setStartTime(entity.getStartTime());
        eventDTO.setEndTime(entity.getEndTime());
        eventDTO.setSubtitle(entity.getSubtitle());
        eventDTO.setCommonActions(this.actionMapper.mapToDTOList(entity.getCommonActions()));
        eventDTO.setPlaceDTO(this.placeMapper.mapToDTO(entity.getPlace().getValue()));
        eventDTO.setFacebookId(entity.getFacebookId());
        return eventDTO;
    }

}
