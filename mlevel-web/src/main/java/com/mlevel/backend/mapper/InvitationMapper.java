package com.mlevel.backend.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.mlevel.backend.api.InvitationDTO;
import com.mlevel.backend.service.model.Event;
import com.mlevel.backend.service.model.Invitation;
import com.mlevel.backend.service.model.User;

@Singleton
public class InvitationMapper extends AbstractMapper<Invitation, InvitationDTO> {

	@Inject
	private ActionMapper actionMapper;

	@Override
	public Invitation mapToEntity(InvitationDTO dto) {
		Invitation invitation = new Invitation();
		invitation.setId(dto.id);
		invitation.setAction(actionMapper.mapToEntity(dto.action));
		invitation.setTimestamp(dto.timestamp);
		invitation.setEndTime(dto.endTime);
		invitation.setStartTime(dto.startTime);
		if(dto.eventId != null) {
			invitation.setEvent(Ref.create(Key.create(Event.class, dto.eventId)));
		}
		invitation.setReceiver(Ref.create(Key.create(User.class, dto.receiverId)));
		invitation.setSender(Ref.create(Key.create(User.class, dto.senderId)));
		invitation.setState(Invitation.State.valueOf(dto.state.name()));
		return invitation;
	}

	@Override
	public InvitationDTO mapToDTO(Invitation entity) {
		InvitationDTO invitationDTO = new InvitationDTO();
		if(entity.getEvent() != null) {
			invitationDTO.eventId = entity.getEvent().getKey().getId();
		}
		invitationDTO.action = actionMapper.mapToDTO(entity.getAction());
		invitationDTO.endTime = entity.getEndTime();
		invitationDTO.startTime = entity.getStartTime();
		invitationDTO.senderId = entity.getSender().getKey().getId();
		invitationDTO.receiverId = entity.getReceiver().getKey().getId();
		invitationDTO.timestamp = entity.getTimestamp();
		invitationDTO.state = InvitationDTO.State.valueOf(entity.getState().name());
		invitationDTO.id = entity.getId();
		return invitationDTO;
	}
}
