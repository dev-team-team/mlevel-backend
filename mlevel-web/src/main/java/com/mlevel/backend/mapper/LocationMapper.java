package com.mlevel.backend.mapper;

import com.google.inject.Singleton;
import com.mlevel.backend.api.location.LocationDTO;
import com.mlevel.backend.service.model.Location;

@Singleton
public class LocationMapper extends AbstractMapper<Location, LocationDTO> {


    @Override
    public Location mapToEntity(LocationDTO dto) {
        Location location = new Location();
        location.setAddress(dto.getAddress());
        location.setCity(dto.getCity());
        location.setCountry(dto.getCountry());
        location.setLatitude(dto.getLatitude());
        location.setLongitude(dto.getLongitude());
        return location;
    }

    @Override
    public LocationDTO mapToDTO(Location entity) {
        LocationDTO dto = new LocationDTO();
        dto.setCountry(entity.getCountry());
        dto.setCity(entity.getCity());
        dto.setAddress(entity.getAddress());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        return dto;
    }

}
