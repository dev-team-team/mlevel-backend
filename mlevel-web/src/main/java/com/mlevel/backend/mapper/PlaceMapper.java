package com.mlevel.backend.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.api.location.PlaceDTO;
import com.mlevel.backend.service.model.Place;

@Singleton
public class PlaceMapper extends AbstractMapper<Place, PlaceDTO> {

    @Inject
    private LocationMapper locationMapper;

    @Override
    public Place mapToEntity(PlaceDTO dto) {
        Place place = new Place();
        place.setName(dto.getName());
        place.setFacebookId(dto.getFacebookId());
        place.setLocation(locationMapper.mapToEntity(dto.getLocation()));
        return place;
    }

    @Override
    public PlaceDTO mapToDTO(Place entity) {
        PlaceDTO dto = new PlaceDTO();
        dto.setFacebookId(entity.getFacebookId());
        dto.setName(entity.getName());
        dto.setLocation(locationMapper.mapToDTO(entity.getLocation()));
        return dto;
    }

}
