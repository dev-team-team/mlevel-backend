package com.mlevel.backend.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.api.profile.ProfileDTO;
import com.mlevel.backend.service.model.Profile;

@Singleton
public class ProfileMapper extends AbstractMapper<Profile, ProfileDTO> {

	@Inject
	private CategoryMapper categoryMapper;

	@Inject
	private DiscoveryPreferencesMapper discoveryPreferencesMapper;

	@Override
	public Profile mapToEntity(ProfileDTO dto) {
		Profile profile = new Profile();
		profile.setId(dto.getId());
		profile.setNickName(dto.getNickName());
		profile.setShortMessage(dto.getShortMessage());
		if(dto.getCategories() != null) {
			profile.setCategories(categoryMapper.mapToEntityList(dto.getCategories()));
		}

		if(dto.getDiscoveryPreferencesDTO() != null){
			profile.setDiscoveryPreferences(discoveryPreferencesMapper.mapToEntity(dto.getDiscoveryPreferencesDTO()));
		}
		profile.setSex(dto.getSex());
		return profile;
	}

	@Override
	public ProfileDTO mapToDTO(Profile entity) {
		ProfileDTO profile = new ProfileDTO();
		profile.setId(entity.getId());
		profile.setNickName(entity.getNickName());
		profile.setShortMessage(entity.getShortMessage());
		if(entity.getCategories() != null) {
			profile.setCategories(categoryMapper.mapToDTOList(entity.getCategories()));
		}
		if(entity.getDiscoveryPreferences() != null) {
			profile.setDiscoveryPreferencesDTO(discoveryPreferencesMapper.mapToDTO(entity.getDiscoveryPreferences()));
		}
		profile.setSex(entity.getSex());
		return profile;

	}

}
