package com.mlevel.backend.security;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Authenticator;
import com.google.inject.Singleton;
import com.mlevel.backend.service.AuthorizationTokenService;
import com.mlevel.backend.service.model.authentication.AuthorizationToken;

import javax.servlet.http.HttpServletRequest;

@Singleton
public class CustomAuthenticator implements Authenticator {

	protected static final String HEADER_AUTHORIZATION = "Authorization";

	private AuthorizationTokenService tokenService = new AuthorizationTokenService();

	@Override
	public User authenticate(HttpServletRequest httpRequest) {
		String authToken = httpRequest.getHeader(HEADER_AUTHORIZATION);
		AuthorizationToken authorizationToken = tokenService.loadAuthorizationToken(authToken);
		if(authorizationToken != null && !authorizationToken.hasExpired()){
			com.mlevel.backend.service.model.User user = authorizationToken.getUser().getValue();
			return new User(user.getId().toString(), user.getEmail());
		}
		return null;
	}

}
