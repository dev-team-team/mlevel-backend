package com.mlevel.backend.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.inject.Singleton;
import com.mlevel.backend.api.blob.ImageUploadedDTO;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by agustin on 20/05/16.
 */
@Singleton
public class ImageUploadedServlet extends HttpServlet {

	BlobstoreService blobstoreService = BlobstoreServiceFactory
		.getBlobstoreService();

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		List<BlobKey> blobs = blobstoreService.getUploads(req).get("file");
		BlobKey blobKey = blobs.get(0);

		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		ServingUrlOptions servingOptions = ServingUrlOptions.Builder.withBlobKey(blobKey);

		String servingUrl = imagesService.getServingUrl(servingOptions);

		resp.setStatus(HttpServletResponse.SC_OK);
		resp.setContentType("application/json");
		ImageUploadedDTO imageUploadedDTO = new ImageUploadedDTO();
		imageUploadedDTO.setBlobKey(blobKey.getKeyString());
		imageUploadedDTO.setServingUrl(servingUrl);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(imageUploadedDTO);

		PrintWriter out = resp.getWriter();
		out.print(json);
		out.flush();
		out.close();
	}
}
